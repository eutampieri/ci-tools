#!/usr/bin/env python3

import json
import sys
from utils import *

events = [json.loads(i) for i in sys.stdin.readlines()]
total = 0
passed = 0
for event in events:
    if event["type"] == "suite" and event["event"] == "started":
        total += event["test_count"]
    elif event["type"] == "suite" and event["event"] == "ok":
        passed += event["passed"]
name = sys.argv[1]
download_badge("["+name.replace("_","__")+"] tests", str(passed)+"/"+str(total), get_colour(float(passed)/float(total)*100), "test_"+name+".svg")

