#!/usr/bin/env python3

from urllib.parse import quote
from urllib.request import Request, urlopen

def get_colour(percentage):
        steps = {
                90: "brightgreen",
                80: "green",
                70: "yellowgreen",
                50: "yellow",
                30: "orange",
                0: "red"
        }
        for pct, colour in steps.items():
            if percentage >= pct:
                return colour

def download_badge(label, content, colour, path):
    url = "https://img.shields.io/badge/"+quote(label)+"-"+quote(content)+"-"+colour
    badge = urlopen(Request(url, headers={"User-Agent": "Mozilla/5.0 (X11; U; Linux i686) Gecko/20071127 Firefox/2.0.0.11"})).read()
    f = open(path, 'wb')
    f.write(badge)
    f.close()
