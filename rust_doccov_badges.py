#!/usr/bin/env python3

import json
import sys
from utils import *

workspaces = [json.loads(i) for i in sys.stdin.readlines()]
workspace = workspaces[0]
total = 0
documented = 0
for i in workspace.values():
    total += i["total"]
    documented += i["with_docs"]
documented = float(documented)/float(total) * 100.0
download_badge("doc", str(int(documented)
                          ) + " %", get_colour(documented), "doc.svg")
